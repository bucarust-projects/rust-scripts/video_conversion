use std::{env, path::Path};

use video_conversion::convert_file::get_video_files;

fn main() {
    let args: Vec<String> = env::args().collect(); 
    let conversion_path = Path::new(&args[1]);

    // let parent_folder = match env::var_os("HOME") {
    //     Some(home) => Path::new(&home)
    //         .join("Downloads")
    //         .join("All_Downloads")
    //         .join("vidCourses")
    //         .join("Internet Security A Hands-on Approach"),
    //     None => panic!("$HOME is not set"),
    // };
    get_video_files(conversion_path);
}
