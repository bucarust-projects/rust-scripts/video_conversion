use std::path::{Path, PathBuf};

use crate::cmd_file;

pub fn get_video_files(parent_folder: &Path) -> () {
    let _course_folders: () = parent_folder
        .read_dir()
        .expect("No folder found")
        .map(|file_path| match file_path {
            Ok(file) => {
                let file = file.path();
                let _match_file: () = match file.is_dir() {
                    true => {
                        if let true = file.exists() {
                            get_video_files(&file);
                        };
                    }
                    false => {
                        if let Some(video_file) = filter_video_files(&file, FileExtension::MP4) {
                            let ffmpeg_output = video_file.with_extension("mkv");
                            // println!("FILE: {:#?}", &ffmpeg_output);
                            //println!("FILE: {:#?}", &video_file);
                            let _: Result<(), ()> =
                                match cmd_file::run_ffmpeg_cmd(&video_file, &ffmpeg_output) {
                                    Ok(()) => Ok(()),
                                    Err(_) => panic!("No files found"),
                                };
                            cmd_file::run_rm_rf_mp4_files_cmd(&video_file);
                        }
                    }
                };
            }
            Err(_) => {
                panic!("No file found")
            }
        })
        .collect();
    println!("END CONTENT OF THE COURSE: {:#?}", parent_folder);
}

enum FileExtension {
    MP4,
    WMV,
}

fn filter_video_files(video_file: &Path, file_format: FileExtension) -> Option<PathBuf> {
    match file_format {
        FileExtension::MP4 => {
            let mp4_files: Option<PathBuf> = match video_file.extension() {
                Some(ext) => match ext.to_str().unwrap() {
                    "mp4" => Some(video_file.to_path_buf()),
                    _ => None,
                },
                None => None,
            };
            return mp4_files;
        }
        FileExtension::WMV => {
            let wmv_files: Option<PathBuf> = match video_file.extension() {
                Some(ext) => match ext.to_str().unwrap() {
                    "wmv" => Some(video_file.to_path_buf()),
                    _ => None,
                },
                None => None,
            };
            return wmv_files;
        }
    }
}
