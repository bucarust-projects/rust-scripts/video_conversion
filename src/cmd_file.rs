use std::{path::Path, process::Command};

pub fn run_ffmpeg_cmd(input: &Path, output: &Path) -> Result<(), ()> {
    Command::new("ffmpeg")
        .arg("-i")
        .arg(&input)
        .arg("-vf")
        .arg("scale=1280x720")
        .arg("-vcodec")
        .arg("libx264")
        .arg("-preset")
        .arg("slow")
        .arg("-crf")
        .arg("32")
        .arg(&output)
        .status()
        .expect("Ffmpeg seems not responding");

    Ok(())
}

pub fn run_rm_rf_mp4_files_cmd(mp4_file: &Path) -> () {
    Command::new("rm")
        .arg("-rf")
        .arg(&mp4_file)
        .status()
        .expect("Ffmpeg seems not responding");
}
